# Requirements Modeling Project

This is an Eclipse project that implements a Requirements Model.

To build execute in the command line:

	mvn clean install
	
# Note

This is not a "complete" Requirements Model project. It only includes a simple metamodel (without validations and other concerns). It contains the minimum to be used in conjunction with the [pt.isep.edom.atl.example](https://bitbucket.org/mei-isep/pt.isep.edom.atl.example)  